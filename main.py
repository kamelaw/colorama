# https://pypi.org/project/colorama/
# https://www.youtube.com/watch?v=u51Zjlnui4Y
import colorama
from colorama import Fore, Back, Style
colorama.init(autoreset=True)

print('\033[31m' + 'some red text')
print('\033[39m')  # reset to default color
print()
print(f"{Fore.RED}C{Fore.GREEN}O{Fore.YELLOW}L{Fore.BLUE}O{Fore.MAGENTA}R{Fore.CYAN}")
print(f"{Fore.RED}RED TEXT")
print(f"{Fore.GREEN}GREEN TEXT")
print(F"{Fore.YELLOW}YELLOW TEXT")
print(f"{Fore.BLUE} BLUE TEXT")
print(f"{Fore.MAGENTA}MAGENTA TEXT")


